export PATH="$HOME/.npm-packages/bin:$PATH"

# NPM commands you install globally are usable | https://gist.github.com/DanHerbert/9520689
export PATH="$HOME/.node/bin:$PATH"
