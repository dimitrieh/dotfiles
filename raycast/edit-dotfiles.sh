#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Edit dotfiles
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🤖

# Documentation:
# @raycast.description Edit your dotfiles directly in VScode
# @raycast.author Dimitrie Hoekstra
# @raycast.authorURL dimitr.ie

code ~/.dotfiles
