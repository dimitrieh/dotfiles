# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/dimitrie/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/dimitrie/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/dimitrie/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/dimitrie/google-cloud-sdk/completion.zsh.inc'; fi
