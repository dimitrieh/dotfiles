export PATH="./bin:/usr/local/bin:/usr/local/sbin:$ZSH/bin:$PATH"
export MANPATH="/usr/local/man:/usr/local/mysql/man:/usr/local/git/man:$MANPATH"

# To use GNU core utilities with their normal names | https://www.gnu.org/software/coreutils
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
